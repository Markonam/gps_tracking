<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DriverResource;
use App\Http\Resources\LocationResource;
use App\Location;
use App\User;
use Illuminate\Http\Request;

class LocationsController extends Controller
{
    public function index()
    {
        $this->authorize('drivers');
        $location = Location::where('user_id', auth()->id())->first();
        return new LocationResource($location);
    }
    public function create(Request $request)
    {
        $this->authorize('drivers');
        $this->validate($request, [
            'longitude' => 'required',
            'latitude' => 'required',
        ]);
        $location = Location::create([
            'user_id' => auth()->id(),
            'longitude' => $request->longitude,
            'latitude' => $request->latitude,
        ]);
        return new LocationResource($location);
    }

    public function update(Request $request)
    {
        $this->authorize('drivers');
        $this->validate($request, [
            'longitude' => 'required',
            'latitude' => 'required',
        ]);

        $location = Location::where('user_id', auth()->id())->first();

        Location::where('user_id', auth()->id())
            ->update([
                'longitude' => $request->longitude,
                'latitude' => $request->latitude,
            ]);

        return (new LocationResource($location))->additional([
            'message' => [
                'status' => 'success update',
            ],
        ]);
    }
}
