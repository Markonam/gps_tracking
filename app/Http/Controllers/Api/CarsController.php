<?php

namespace App\Http\Controllers\Api;

use App\Car;
use App\Http\Controllers\Controller;
use App\Http\Resources\CarResource;
use Illuminate\Http\Request;

class CarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('drivers');
        $cars = Car::where('user_id', auth()->id())->get();

        return CarResource::collection($cars);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('drivers');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('drivers');
        $this->validate($request, [
            'merk' => 'required|string',
            'plat_number' => 'required|string|min:3|max:12',
            'price' => 'required',
        ]);

        $car = Car::create([
            'user_id' => auth()->id(),
            'merk' => $request->merk,
            'plat_number' => $request->plat_number,
            'price' => $request->price,
        ]);

        return new CarResource($car);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        $this->authorize('drivers');
        $auth = Car::where([
            'id' => $car->id,
            'user_id' => auth()->id(),
        ])->first();
        return new CarResource($auth);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        $this->authorize('drivers');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        $this->authorize('drivers');
        $this->validate($request, [
            'merk' => 'required|string',
            'plat_number' => 'required|string|min:3|max:12',
            'price' => 'required',
        ]);

        $auth = Car::where([
            'id' => $car->id,
            'user_id' => auth()->id(),
        ])->first();

        Car::where('id', $auth->id)
            ->update([
                'merk' => $request->merk,
                'plat_number' => $request->plat_number,
                'price' => $request->price,
            ]);

        return (new CarResource($auth))->additional([
            'message' => [
                'status' => 'success update',
            ],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        $this->authorize('drivers');
        $auth = Car::where([
            'id' => $car->id,
            'user_id' => auth()->id(),
        ])->first();
        Car::destroy($auth->id);
        return (new CarResource($car))->additional([
            'message' => [
                'status' => 'success delete',
            ],
        ]);
    }
}
