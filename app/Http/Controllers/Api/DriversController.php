<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\DriverResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use File;
use JWTAuth;

class DriversController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('operators');
        $drivers = User::where('role_id', 2)->get();
        return DriverResource::collection($drivers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('operators');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('operators');
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'phone' => 'required|string|min:10|max:13',
            'photo' => 'required|file|image|mimes:jpeg,png,jpg',
        ]);

        $file = $request->file('photo');
        $nama_file = time() . ' ' . $file->getClientOriginalName();
        $tujuan_opload = 'photo';
        $file->move($tujuan_opload, $nama_file);

        $driver = User::create([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'photo' => $nama_file,
            'password' => Hash::make($request->get('password')),
            'role_id' => 2,
        ]);

        $token = JWTAuth::fromUser($driver);

        return (new DriverResource($driver))->additional([
            'meta' => [
                'token' => $token,
            ],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('operators');
        $driver = User::where([
            'id' => $id,
            'role_id' => 2,
        ])->first();
        return new DriverResource($driver);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('operators');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('operators');
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|min:10|max:13',
            'photo' => 'required|file|image|mimes:jpeg,png,jpg',
        ]);

        $driver = User::where([
            'id' => $id,
            'role_id' => 2,
        ])->first();

        File::delete('photo/' . $driver->photo);
        $file = $request->file('photo');
        if ($file) {
            $nama_file = time() . ' ' . $file->getClientOriginalName();
            $tujuan_opload = 'photo_drivers';
            $file->move($tujuan_opload, $nama_file);
            User::where('id', $driver->id)
                ->update([
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'photo' => $nama_file,
                ]);
        } else {
            User::where('id', $driver->id)
                ->update([
                    'name' => $request->name,
                    'phone' => $request->phone,
                ]);
        }

        return (new DriverResource($driver))->additional([
            'message' => [
                'status' => 'success update',
            ],
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('operators');
        $driver = User::where([
            'id' => $id,
            'role_id' => 2,
        ])->first();
        File::delete('photo/' . $driver->photo);
        User::destroy($driver->id);

        return (new DriverResource($driver))->additional([
            'message' => [
                'status' => 'success delete',
            ],
        ]);
    }
}
