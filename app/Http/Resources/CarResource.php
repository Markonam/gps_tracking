<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'merk' => $this->merk,
            'price' => $this->price . '/hours',
            'plat_number' => $this->plat_number,
            'last_update' => $this->updated_at->diffForHumans(),
            'drivers' => new DriverResource($this->user),
        ];
    }
}
