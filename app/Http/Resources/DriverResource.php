<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        switch ($this->role_id) {
            case 1:
                $role = 'operators';
                break;
            case 2:
                $role = 'drivers';
                break;
            case 3:
                $role = 'users';
                break;

            default:
                $role = 'Maaf Bug';
                break;
        }
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'number_phone' => $this->phone,
            'name_photo' => $this->photo,
            'joined' => $this->created_at->diffForHumans(),
            'roles' => $role,
            'in_location' => new LocationResource($this->location),
        ];
    }
}
