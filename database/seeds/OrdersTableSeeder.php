<?php

use App\Order;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Order::create([
            'user_id' => 3,
            'address' => 'Gang Rambutan, RT 01, RW 01, Wagir, Malang',
            'car_id' => 1,
            'price' => 1000000,
            'time' => 2,
        ]);
    }
}
