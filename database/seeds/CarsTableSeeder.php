<?php

use App\Car;
use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Car::create([
            'user_id' => 2,
            'merk' => 'Avanza',
            'plat_number' => 'N 8766 EEK',
            'price' => 50000,
        ]);
        Car::create([
            'user_id' => 2,
            'merk' => 'Xenia',
            'plat_number' => 'N 8799 EEM',
            'price' => 100000,
        ]);
    }
}
