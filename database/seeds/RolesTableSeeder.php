<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'operators'
        ]);
        Role::create([
            'name' => 'drivers'
        ]);
        Role::create([
            'name' => 'users'
        ]);
    }
}
