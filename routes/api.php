<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');
Route::get('all', 'Api\UserController@index');
Route::get('user', 'Api\UserController@getAuthenticatedUser')->middleware('jwt.verify');
Route::get('drivers', 'Api\DriversController@index')->middleware('jwt.verify');
Route::post('drivers', 'Api\DriversController@store')->middleware('jwt.verify');
Route::get('drivers/create', 'Api\DriversController@create')->middleware('jwt.verify');
Route::get('drivers/{id}', 'Api\DriversController@show')->middleware('jwt.verify');
Route::patch('drivers/{id}', 'Api\DriversController@update')->middleware('jwt.verify');
Route::delete('drivers/{id}', 'Api\DriversController@destroy')->middleware('jwt.verify');
Route::get('drivers/{id}/edit', 'Api\DriversController@edit')->middleware('jwt.verify');
Route::post('locations', 'Api\LocationsController@create')->middleware('jwt.verify');
Route::get('locations', 'Api\LocationsController@index')->middleware('jwt.verify');
Route::patch('locations', 'Api\LocationsController@update')->middleware('jwt.verify');
Route::resource('cars', 'Api\CarsController')->middleware('jwt.verify');
Route::resource('orders', 'Api\OrdersController')->middleware('jwt.verify');
